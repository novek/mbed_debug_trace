#include "debug_trace.h"

void debug_trace_init(){
    (mbed_trace_init)();
}

void debug_trace_mutex_lock_function_set(void (*mutex_lock_f)(void)){
    (mbed_trace_mutex_wait_function_set)(mutex_lock_f);
}

void debug_trace_mutex_unlock_function_set(void (*mutex_unlock_f)(void)){
    (mbed_trace_mutex_release_function_set)(mutex_unlock_f);
}


void debug_tracef(uint8_t dlevel, const char *grp, const char *fmt, ...){
    if(strcmp(grp,COMPONENT_NOT_DEFINED)==0){
        return;
    }
    va_list ap;
    va_start(ap, fmt);
    (mbed_vtracef)(dlevel, grp, fmt, ap);
    va_end(ap);
}