#ifndef NU_DEBUG_TRACE_H_
#define NU_DEBUG_TRACE_H_

#include "mbed.h"
#include "mbed_trace.h"

#define COMPONENT_NOT_DEFINED "NOT_DEFINED"
#define DEBUG_GROUP COMPONENT_NOT_DEFINED

#ifndef MBED_CONF_DEBUG_TRACE_LEVEL
#define MBED_CONF_DEBUG_TRACE_LEVEL TRACE_LEVEL_INFO
#endif

#define debug_tracef(...)

//usage macros:
#if MBED_CONF_DEBUG_TRACE_LEVEL >= TRACE_LEVEL_DEBUG
#define trace_debug(x,...)\
    debug_tracef(\
    TRACE_LEVEL_DEBUG,\
    DEBUG_GROUP,\
    "[%" PRIu64"][%s:%d] " x,\
    timer_read_us(global_timer),\
    __FILE__, __LINE__, ##__VA_ARGS__); 
#else
#define trace_debug(x,...)
#endif

#if MBED_CONF_DEBUG_TRACE_LEVEL >= TRACE_LEVEL_INFO
#define trace_info(x,...)\
    debug_tracef(\
    TRACE_LEVEL_INFO,\
    DEBUG_GROUP,\
    "[%" PRIu64"][%s:%d] " x,\
    timer_read_us(global_timer),\
    __FILE__, __LINE__, ##__VA_ARGS__);   
#define trace_at_send(x, ...)\
    debug_tracef(\
        TRACE_LEVEL_INFO,\
        DEBUG_GROUP,\
        "[AT->][%" PRIu64"] " x "\r\n",\
        timer_read_us(global_timer),\
        ##__VA_ARGS__\
    );

#define trace_at_recv(x, ...)\
    debug_tracef(\
        TRACE_LEVEL_INFO,\
        DEBUG_GROUP,\
        "[AT<-][%" PRIu64"] " x "\r\n",\
        timer_read_us(global_timer),\
        ##__VA_ARGS__\
    );
    
#else
#define trace_info(x,...)
#define trace_at_send(x, ...)
#define trace_at_recv(x, ...)
#endif

#if MBED_CONF_DEBUG_TRACE_LEVEL >= TRACE_LEVEL_WARN
#define trace_warn(x,...)\
    debug_tracef(\
    TRACE_LEVEL_WARN,\
    DEBUG_GROUP,\
    "[%" PRIu64"][%s:%d] " x,\
    timer_read_us(global_timer),\
    __FILE__, __LINE__, ##__VA_ARGS__);   //!< Print debug message   //!< Print warning message
#else
#define trace_warn(x,...)
#endif

#if MBED_CONF_DEBUG_TRACE_LEVEL >= TRACE_LEVEL_ERROR
#define trace_error(x,...)\
    debug_tracef(\
    TRACE_LEVEL_ERROR,\
    DEBUG_GROUP,\
    "[%" PRIu64"][%s:%d] " x,\
    timer_read_us(global_timer),\
    __FILE__, __LINE__, ##__VA_ARGS__);   //!< Print Error Message
#else
#define trace_error(x,...)

#endif

//prob not thread safe
#define trace_printf(x, ...)\
    printf(x, ##__VA_ARGS__);

#define trace_cmd(x, ...)\
    debug_tracef(TRACE_LEVEL_CMD,DEBUG_GROUP, x, ##__VA_ARGS__);



    



void debug_trace_init();

#undef debug_tracef
void debug_tracef(uint8_t dlevel, const char *grp, const char *fmt, ...);

void debug_trace_mutex_lock_function_set(void (*mutex_lock_f)(void));

void debug_trace_mutex_unlock_function_set(void (*mutex_unlock_f)(void));

#endif