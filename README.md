## Debug Trace

A customized extension of [mbed_trace](https://github.com/ARMmbed/mbed-trace)

You do not need to enable `mbed_trace` to use debug_trace.

To use `debug_trace` in a source you must first define `DEBUG_GROUP`, in the source file (not the header).

**`DEBUG_GROUP` must be of length `4`**


```cpp
#include "mbed.h"
#include "debug_trace.h"
#define DEBUG_GROUP "MAIN" 


Mutex debug_mutex;

void lock_debug_mutex(){
    debug_mutex.lock();
}
void unlock_debug_mutex(){
    debug_mutex.unlock();
}


int main(){
    //enable tracing
    debug_trace_init();
    //set mutex 
    debug_trace_mutex_lock_function_set(&lock_debug_mutex);
    debug_trace_mutex_unlock_function_set(&unlock_debug_mutex);
    //log
    trace_info("Hello world");
    while(1){

    }
}
```

```
[INFO][MAIN][197021][./src/main.cpp:7] Hello World
```

Supported trace statements are
```
trace_debug
trace_info
trace_warn
trace_error
trace_cmd
trace_printf
``` 

To disable logging in component undef or remove definition of `DEBUG_GROUP`.

You can set the trace level using by setting `debug_trace.level` in `mbed_app.json`
optional values are `TRACE_LEVEL_ERROR,TRACE_LEVEL_WARN,TRACE_LEVEL_INFO,TRACE_LEVEL_DEBUG` default value is `TRACE_LEVEL_DEBUG`.

```json
{
    "debug_trace.level":"TRACE_LEVEL_DEBUG"
}
```